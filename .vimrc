" Functions
function! MapToggle(key, opt)
	let cmd = ':set '.a:opt.'! \| set '.a:opt."?\<CR>"
	exec 'nnoremap '.a:key.' '.cmd
endfunction
command -nargs=+ MapToggle call MapToggle(<f-args>)

function! ToggleCC()
	if &cc == ''
		set cc=81,101,121
	else
		set cc=
	endif
endfunction

function! ToggleNumbers()
	if &number
		:set nonumber norelativenumber foldcolumn=0 nolist
	else
		:set number relativenumber foldcolumn=3 list
	end
endfunction

function! FormatLog()
	% s/\\\\/\\/g
	% s/\\n/\r/g
	% s/:\ /:\r\ \ \ \ /g
	syn match Comment "\[.*\]"
	syn region Boolean start="SP\\" end=" in"ms=s-3 end="$"
	syn region File start="/" end=" " end="$" end="\.php"
	syn match Label "\(\d\+\)"
	syn match Label ":\d\+"ms=s+1
	syn match Label "line\s*\d\+"ms=s+4
	highlight File ctermfg=green
endfunction

function! SuperWrite(Q)
	silent write !sudo tee %
	edit!
	if a:Q == "true"
		quit
	endif
endfunction

function! ToggleTrimWhitespace()
	if exists('b:noTrimWhitespace')
		let b:noTrimWhitespace=
	else
		let b:noTrimWhitespace=1
	endif
endfunction

function! TrimWhitespace()
	if exists('b:noTrimWhitespace')
		return
	endif
	let l:save = winsaveview()
	keeppatterns %s/\s\+$//e
	call winrestview(l:save)
endfunction

function! MusicTitles()
	%s/^/\=printf('%02d ', line('.'))
	%s/$/\.mp3/
endfunction

function! SumSelection() range abort
	let sum = 0
	let [y1, x1] = getpos("'<")[1:2]
	let [y2, x2] = getpos("'>")[1:2]
	let lines = getline(y1, y2)
	if len(lines) > 0
		for line in lines
			let sum += str2float(substitute(trim(line[x1 - 1:x2 - 1]), ',', '.', 'g'))
		endfor
	endif
	echom(sum)
endfunction

:let mapleader = " "

function! VimdiffSettings()
	set wrap
	nnoremap J 10gj
	nnoremap K 10gk
endfunction

function! VimdiffSetup()
	if &diff
		:call VimdiffSettings()
		:exe 'normal \<CR>\<C-w>\<C-w>>'
		:call VimdiffSettings()
		:exe 'normal \<CR>\<C-w>\<C-w>>'
		nnoremap <F12> :call VimdiffSettings()<CR><C-w><C-w>:call VimdiffSettings()<CR><C-w><C-w>
	endif
endfunction

" Mappings
MapToggle <F1> hlsearch
nnoremap <F2> :call ToggleCC()<CR>
nnoremap <F9> :call ToggleNumbers()<CR>
nnoremap <F10> :call ToggleTrimWhitespace()<CR>

" automaticaly toggle paste when inserting (not quite sure how/why it works..)
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"
inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()
function! XTermPasteBegin()
	set pastetoggle=<Esc>[201~
	set paste
	return ""
endfunction

nnoremap <C-J> :resize +1<CR>
nnoremap <C-K> :resize -1<CR>
nnoremap <C-L> :vertical resize +1<CR>
nnoremap <C-H> :vertical resize -1<CR>

" add relative line movements to the jump list
nnoremap <expr> j (v:count > 1 ? "m'" . v:count : "") . 'j'
nnoremap <expr> k (v:count > 1 ? "m'" . v:count : "") . 'k'

" move lines up/down
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
inoremap <C-j> <C-O>:m .+1<CR><C-O>==
inoremap <C-k> <C-O>:m .-2<CR><C-O>==

nnoremap <leader>gh :diffget //3<CR>
nnoremap <leader>gl :diffget //2<CR>
nnoremap <leader>gs :G<CR>

nnoremap Y yg_

" sort paragraph lines alphabeticaly
nnoremap <leader>s Vip:sort<CR>

" go to reference (in new split)
nnoremap gt g<C-]>
nnoremap gT <C-w>g}

autocmd VimEnter * call VimdiffSetup()
autocmd BufWritePre * call TrimWhitespace()

command FormatLog call FormatLog()
command MusicTitles call MusicTitles()
command W call SuperWrite("false")
command WQ call SuperWrite("true")
command Date .-1r!date +\%d.\%m.\%g

" reserve <C-r> mappings, that would be overwritten otherwise
cnoremap <C-r>" <C-r>"
cnoremap <C-r>% <C-r>%
cnoremap <C-r># <C-r>#
cnoremap <C-r>* <C-r>*
cnoremap <C-r>+ <C-r>+
cnoremap <C-r>/ <C-r>/
cnoremap <C-r>: <C-r>:
cnoremap <C-r>- <C-r>-
cnoremap <C-r>. <C-r>.
cnoremap <C-r>= <C-r>=
cnoremap <C-r>0 <C-r>0
cnoremap <C-r>1 <C-r>1
cnoremap <C-r>2 <C-r>2
cnoremap <C-r>3 <C-r>3
cnoremap <C-r>4 <C-r>4
cnoremap <C-r>5 <C-r>5
cnoremap <C-r>6 <C-r>6
cnoremap <C-r>7 <C-r>7
cnoremap <C-r>8 <C-r>8
cnoremap <C-r>9 <C-r>9

" command mode mappings to be bash-like
cnoremap <C-a> <C-b>
cnoremap <ESC><BS> <C-w>
cnoremap <C-p> <Up>
cnoremap <C-n> <Down>
cnoremap <C-j> <CR>
cnoremap <C-r> <C-f>?
cnoremap <C-l> <C-d>
cnoremap <C-d> <Del>
cnoremap <C-b> <Left>
cnoremap <C-f> <Right>
cnoremap <ESC>b <C-\>eCmdMoveBackWord()<CR>
cnoremap <ESC>f <C-\>eCmdMoveForwardWord()<CR>

function CmdMoveBackWord()
	let cmd = getcmdline()
	let split_pos = getcmdpos() - 2
	if split_pos <= 1
		call setcmdpos(1)
		return cmd
	endif
	let left = cmd[:split_pos]
	let pos = strlen(split(left, '\>[^$]\(.\+\>[^$]\)\@!\zs', 1)[0])
	call setcmdpos(pos)
	return cmd
endfunction

function CmdMoveForwardWord()
	let cmd = getcmdline()
	let pos = getcmdpos()
	let right = cmd[pos:]
	let offset = strlen(split(right, '[\>\/\\\|\.:_,;\-+]\zs', 1)[0])
	call setcmdpos(pos + offset)
	return cmd
endfunction

" SPML, HTML, XML, XSL, XSLT
augroup markup_mappings
	autocmd!
	autocmd FileType spml,html,xml,xsl,xslt nmap <expr> <leader>< "a<" . input("Tag-Name: ") . " >\<Esc>T<yt f>a</>\<Esc>hpF>dh"
	autocmd FileType spml,html,xml,xsl,xslt nmap <expr> <leader>> "a<" . input("Tag-Name: ") . "/>\<Esc>"
	autocmd FileType spml,html,xml,xsl,xslt nmap <expr> <leader><Space> "a\<CR>\<CR>\<Esc>kA\<Tab>\<Esc>"
	autocmd FileType spml,html,xml,xsl,xslt nnoremap <expr> <cr> getline('.')[col('.') - 1:col('.')] == '><' ? 'a<cr><c-o>O': '<cr>'
augroup END
augroup spml_mappings
	autocmd!
	autocmd FileType spml nmap <expr> <leader>/ "a<%--  --%>\<Esc>5h"
	autocmd FileType spml setlocal commentstring=<%--\ %s\ --%>
augroup END

" Rmarkdown
augroup rmarkdown_mappings
	autocmd!
	autocmd FileType markdown,rmd map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter>
	autocmd FileType markdown,rmd let b:noTrimWhitespace=1
augroup END

augroup nasm
	autocmd!
	autocmd FileType nasm set ft=nasm
augroup END

augroup xxd
	autocmd!
	autocmd FileType xxd set ft=xxd
augroup END

augroup templates
	autocmd!
	autocmd BufNewFile *.spml 0r /home/schaeper/.vim/templates/skeleton.spml
augroup END

" Highlighting
highlight FoldColumn ctermbg=black ctermfg=white
highlight StatusLine ctermfg=darkgrey ctermbg=white
highlight Visual ctermfg=black
highlight Search ctermfg=black
highlight DiffAdd ctermbg=4
highlight DiffChange ctermbg=5
highlight DiffText ctermbg=173
highlight CursorLineNr ctermfg=White
" Whitespace Characters (vim)
highlight SpecialKey ctermfg=16
" Whitespace Characters (neo-vim)
highlight NonText ctermfg=16

nnoremap <F9> :call ToggleNumbers()<CR>

" Plugins
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'https://gitlab.com/DrWursterich/vim-spml.git'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-surround'
Plugin 'cespare/vim-toml'
call vundle#end()
filetype plugin indent on

let $BASH_ENV="~/.vim_env"

set number
set relativenumber

set noexpandtab
set tabstop=4
set shiftwidth=4
set copyindent
set preserveindent
set autoindent
set nosmarttab

set scrolloff=6

set hlsearch
set incsearch

set nrformats-=octal
set formatoptions+=Bj

set title
set splitbelow
set splitright
set shortmess-=S
set foldcolumn=3
set path=**
set wildmenu
set wildmode=longest:full
set cmdheight=2

set updatetime=50

set laststatus=2
set statusline=%<%f\ %h%m%r%{FugitiveStatusline()}%=%-14.(%l,%c%V%)\ %P

set nofixendofline
set noerrorbells
set directory=$HOME/.vim/swapfiles//

" show whitespace characters
set list
set listchars=eol:\ ,tab:\|->,trail:~,extends:>,precedes:<

" otherwise highlighting breaks
set background=light

syntax on

