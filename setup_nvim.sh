#!/bin/bash

# written for an old laptop installation.
#
# problems:
#   - which php version to choose?
#   - which clangd version to choose?
#   - how to make sure java is installed and in what version?

sudo apt install -y php7.2 php7.2-curl php7.2-xml composer git java clangd-10 make cmake libtool-bin

# install rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
. $HOME/.cargo/env

# clone necessities
dir=$(pwd)
cd $HOME/git/
git clone 'https://github.com/neovim/neovim.git'
git clone 'https://github.com/phpactor/phpactor.git'

# setup neovim
cd neovim
make CMAKE_BUILD_TYPE=RelWithDebInfo
sudo make install

# setup clangd
sudo ln -s "$(which clangd-10)" /usr/local/bin/clangd

# setup rust_analyzer
rustup component add rust-src
rustup component add rust-analyzer

# setup phpactor
cd $HOME/git/phpactor
composer install
sudo chmod a+x ./bin/phpactor
sudo ln -s ./bin/phpactor /usr/local/bin/phpactor

# setup jdtls
sudo curl -fLo /opt/jdtls/jdtls.tar.gz --create-dirs 'https://download.eclipse.org/jdtls/snapshots/jdt-language-server-latest.tar.gz'
sudo chown -R $(whoami): /opt/jdtls/
cd /opt/jdtls
tar -xf jdtls.tar.gz
rm jdtls.tar.gz
sudo chowd $(whoami): ./bin/jdtls
sudo chmod -R a+x ./bin/jdtls
sudo ln -s ./bin/jdtls /usr/local/bin/jdtls

# setup nvim config
mkdir -p $HOME/.config/nvim
cd $HOME/.config/nvim
curl -fLo nvim.tar.gz 'https://gitlab.com/DrWursterich/dotfiles/-/archive/master/dotfiles-master.tar.gz?path=.config/nvim'
tar -xf nvim.tar.gz
mv ./dotfiles-master-.config-nvim/.config/nvim/* .
rm -rf ./dotfiles-master-.config-nvim ./nvim.tar.gz

# setup nvim plugins
curl -fLo ./autoload/plug.vim --create-dirs 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
mkdir plugged
nvim -c PlugInstall -c exit -c exit

cd "$dir"
