#!/bin/bash

from=wav
to=mp3
loglevel=panic
tmp=files_to_convert.txt
		
[[ -f "$tmp" ]] && rm "$tmp";

echo "caching files to convert"
find . -type f -name "*.$from" \
	| sed "s/\.$from\$//g" \
	| while read line; do 
		echo "$line" >> "$tmp"
	done

amount=$(cat "$tmp" | wc -l)
counter=0
echo "converting $amount files from $from to $to"

while read line; do
	artist="$(echo $line | grep -Po '\.[\\\/]\K[^\\\/]+')";
	name="$(echo $line | grep -Po '[\\\/]\K[^\\\/]+$')";
	album="$(echo $line | grep -Po '\.[\\\/][^\\\/]+[\\\/]\K[^\\\/]+')";
	in="$line.$from";
	out="$line.$to";
	counter=$((counter+1));
	echo "[$counter/$amount] converting $artist - $name ($album)";
	[[ -f "$out" ]] && rm "$out";
	ffmpeg -loglevel $loglevel -i "$in" -filter:a "volume=2.5" -ab 320k -map_metadata 0 -id3v2_version 3 "$out" && rm "$in";
done < "$tmp";

rm "$tmp"

