#!/bin/sh

supportedPlayers="vlc spotify"
acceptableStatii="Playing Paused Stopped"
meta=metadata.tmp

inactive="no 🎶"

reset_variables() {
	status=""
	player=""
	artist=""
	title=""
	album=""
}

mpris_control() {
	/home/schaeper/scripts/mpris_control $@
}

containsElement() {
  local match="$1"
  shift
  for e in $@; do [ "$e" = "$match" ] && return 0; done
  return 1
}

update_info() {
	reset_variables
	for supportedPlayer in $supportedPlayers; do
		local playerStatus="$(mpris_control $supportedPlayer get_status 2>/dev/null)"
		if containsElement "$playerStatus" $acceptableStatii; then
			case "$playerStatus" in 
				"Playing")
					status="▶"
					;;
				"Paused")
					status="▮▮"
					;;
				"Stopped")
					status="■"
					;;
			esac
			player=$supportedPlayer
			artist="$(mpris_control $player get_artist)"
			title="$(mpris_control $player get_title)"
			album="$(mpris_control $player get_album)"
		fi
	done;
	return; 
	if [ "$status" = "" ]; then
		url=$(strings ~/.config/google-chrome/Default/Current\ Session | grep -Po "^https:\/\/www\.youtube\.(de|com)\/watch\?v=[^&]+" | tail -n1)
		if [ url = "" ]; then
			return;
		fi
		status="❗"
		player="youtube"
		youtube-dl --print-json --skip-download "$url" > "$meta"
		title=$(jq -r ".title" "$meta")
		rm "$meta"
	fi
}

case $BLOCK_BUTTON in
	1) 
		trash_output=`mpris_control $player pause`
		;;
	3)
		trash_output=`mpris_control $player next`
		;;
	4)
		trash_output=`mpris_control $player increase_volume`
		alternative_text=`printf 'volume:%3u%% ' "$(echo "$(mpris_control $player get_volume)*100" | bc)"`
		;;
	5)
		trash_output=`mpris_control $player decrease_volume`
		alternative_text=`printf 'volume:%3u%% ' "$(echo "$(mpris_control $player get_volume)*100" | bc)"`
		;;
esac

update_info
full_text="$inactive"
if [ "$status" != "" ]; then
	full_text="$title"
	if [ "$artist" != "" ]; then
		full_text="$artist - $full_text"
	fi
	if [ "$album" != "" ]; then
		full_text="$full_text ($album)"
	fi
	if [ "$player" != "" ]; then
		full_text="$full_text via $player"
	fi
	full_text="$status $full_text"
fi

echo "${alternative_text:-}$full_text" | sed 's/\&/\&amp;/g' | sed 's/</\&lt;/g' | sed 's/>/\&gt;/g'

