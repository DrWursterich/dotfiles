#!/bin/sh

echo "🤘"

notify-send "Started wpages..." &

# Custom Countdown-Regexes
DOWNLOAD_FESTIVAL="\b(\d+\s*(days|hrs|mins))(\s*\d+\s*(days|hrs|mins))*\b"
SUMMERBREEZE="\bFestival-Countdown(\s*(Days|Hrs|Min|Sec|\d+)(\s*:)?)*\b"
WACKEN="\bNOCH\s*\d*\s*TAGE\b"
SKILLET="\b\d+h\b"

# Custom Regexes for continuesly changing Content
GRASPOP="Wheel of Death - Agnostic Front|Full speed into Sunday"

result="$(/home/schaeper/repos/wpages/wpages -u -p "s/$DOWNLOAD_FESTIVAL|$SUMMERBREEZE|$WACKEN/COUNTDOWN/g; s/$GRASPOP/CONTENT/g")"

if [ "$result" != "" ]; then
	echo "\n$(date) wpages (i3blocks)\n$result\n\n" >> /var/log/wpages
	echo " News!"
	notify-send "Updated:\
$result" &
fi

