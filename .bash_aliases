#!/bin/bash

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi

# ls
alias ll='ls -halFG'
alias la='ls -hA'
alias l='ls -hCF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Misc
alias cd..='cd ..'
alias xcopy='xclip -sel clip'
alias search='grep -r -I --exclude-dir=.git --exclude-dir=target --exclude-dir=node_modules --exclude-dir=vendor --exclude-dir=*-styleguide --exclude-dir=BS --exclude-dir=cleancode-tldr --exclude=*.xip --exclude=*.svg --exclude=*.vm --exclude=*.prefs --exclude=*.aip --exclude=newsletter.xml --exclude=changes.xml'
# Maven
alias mvn8='export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/"; mvn'
alias mvn11='export JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64/"; mvn'
alias mvn16='export JAVA_HOME="/usr/lib/jvm/java-16-openjdk-amd64/"; mvn'
alias mvn4='$HOME/git/maven/apache-maven-4.0.0-alpha-7/bin/mvn'
alias mvn_watch='inotifywait -e close_write,moved_to,create -mr ./src | while read -r directory events filename; do clear; mvn clean package test -B | grep -v "^\[" || echo "build failed"; done'

# Hasher
alias hasher='java -jar "/etc/hasher/$(ls /etc/hasher/ | grep "hasher-.*\.jar")"'
alias hasher_generate='hasher -sp -sc -get unhashed -sc " => " -hp -set unhashed -r 16 -s'

# Steam Overlay Notifications Position
alias sp='cat /home/schaeper/.steam/resource/styles/steam.styles | grep -Po "Notifications\.PanelPosition\s*\"\K[^\"]*(?=\"\s*\/\/)" --color=never'

# Lyrics of currently playing song from vlc
alias vlc_lyrics='lyrics "$(mpris_control vlc get_artist)" "$(mpris_control vlc get_title)"'

# Shortcuts
alias r='ranger'
alias c='composer'
alias ci='composer install'
alias cu='composer update --ansi'
alias ca='composer analyse'
alias ct='composer test'
alias call='composer all'
alias nvm='nvim'
alias nv='nvim'
alias n='nvim'
alias vim='nvim'
alias vi='nvim'
alias im='nvim'
alias v='nvim'
alias vimdiff='nvim -d'
alias vd='nvim -d'
alias mc='mkdircd'
alias ie='ies-env'
alias f='find'
alias fn='find . -name'
alias ngrep_port='sudo ngrep -d lo -W byline port'
alias python='python3.10'
alias python3='python3.10'
alias g='cd "$(find $HOME/git/ -mindepth 1 -maxdepth 1 -type d -printf "%f\n" | fzf | sed -e "s@^@$HOME/git/@" || dirname)"'
alias i='cd "$(find $HOME/ies-environments/ -mindepth 1 -maxdepth 1 -type d -printf "%f\n" | fzf | sed -e "s@^@$HOME/ies-environments/@" || dirname)"'
alias wdiff_all='while read -r file <&3; do wdiff $file; done 3< <(wdiff changed)'
alias upgrade='sudo apt update -y && apt list --upgradable && echo -en "\nstart upgrade? [Y/n]: " && read answer && ([[ "$answer" =~ ^[nN]$ ]] && echo "aborted" || ([[ "$answer" =~ ^[yY]?$ ]] && (sudo apt upgrade -y && sudo apt autoremove -y) || echo "you what?"))'
alias t='pwd | grep -qE "/src/publish/php" && cd ../../main/webapp || cd ../../publish/php'

# Session Control
alias logout='/usr/bin/gnome-session-quit --logout'
alias suspend='systemctl suspend'
alias power-off='/usr/bin/gnome-session-quit --power-off'

# Utility
alias :q='echo "DU BIST NICHT IN VIM VERDAMMT!"'
alias :qa='echo "DU BIST NICHT IN VIM VERDAMMT!"'
alias :qa!='echo "DU BIST NICHT IN VIM VERDAMMT!"'
alias :x='echo "DU BIST NICHT IN VIM VERDAMMT!"'
alias die='exit'
alias first_command_today='history | grep -Po "^\s*\d+\s+\K`date +%d\\.%m\\.`\d{2}\s+\d{2}:\d{2}\s+[^\s].*$" | head -n1'
alias what_did_i_do_today='history | grep -Po " $(date +%d.%m.%y) \K.*" | less'
alias cal='ncal -b'
alias fix_mouse='gsettings set org.gnome.desktop.peripherals.mouse accel-profile "flat"; xinput --set-prop "Logitech G300s Optical Gaming Mouse" "libinput Accel Profile Enabled" 1, 0; echo -e "\nAND DONT FORGETT TO PUT DPI ON 300 AND SENSITIVITY ON 1000HZ!"'
alias disable_autofocus='sudo v4l2-ctl -d /dev/video0 --set-ctrl=focus_auto=0'

# Functions
function mkdircd() {
	mkdir -p "$1"
	cd "$1"
}
export -f mkdircd

function mvn_create() {
	mvn archetype:generate -DgroupId="$1" -DartifactId="$2" -Dversion=1.0.0-SNAPSHOT -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4
}
export -f mvn_create

function youtube-dl() {
	local current_dir="$(pwd)"
	cd /home/schaeper/git/youtube-dl
	python -m youtube_dl --output "$current_dir/%(title)s.%(ext)s" $@
	cd "$current_dir"
}
export -f youtube-dl

function youtube-dl-mp3() {
	youtube-dl -f bestaudio --extract-audio --audio-format mp3 --audio-quality 0 $@
}
export -f youtube-dl-mp3

function random_line() {
	cat "$1" | head -n$((1+$(($RANDOM%$(cat "$1" | wc -l))))) | tail -n1
}
export -f random_line

to_relative() {
	perl -MFile::Spec -e 'print File::Spec->abs2rel(@ARGV), "\n"' "$2" "$1"
}
export -f to_relative

# make maven work from deeper directories
# and make paths relative and easier to copy
rmvn() {
	local current_dir="$(pwd)";
	until [ -f pom.xml ]; do
 		if [ "$(pwd)" == "/" ]; then
			echo "no pom.xml in path"
			cd "$current_dir"
			return
		fi
		cd ..
	done
	local mvn_executable=$(echo "$PATH" | grep -Po '[^:]+\/mvn')
	local RE=(\/[^\/]+)+\.java:
	${mvn_executable:-/usr/bin/mvn} $@ | while read line; do
		if [[ "$line" =~ $RE ]]; then
			eval "echo \"\${line/$(echo "${BASH_REMATCH}" | sed 's/\//\\\//g')/$(perl -MFile::Spec -e 'print File::Spec->abs2rel(@ARGV)' "$(echo "${BASH_REMATCH}" | grep -Po '^\/([^\/]+\/)*')" "$current_dir" | sed 's/\//\\\//g')\/$(echo "${BASH_REMATCH%:}" | grep -Po '[^\/]+\.java') }\""
		else
			echo "$line"
		fi
	done
	cd "$current_dir"
}
export -f rmvn

