# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ]; then
	PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ]; then
	PATH="$HOME/.local/bin:$PATH"
fi

# set PATH so it uncludes users's scripts folder
if [ -d "$HOME/scripts" ]; then
	PATH="$HOME/scripts:$PATH"
fi

# custom build neo-vim
if [ -d "$HOME/neovim/bin" ]; then
	PATH="$HOME/neovim/bin:$PATH"
fi

# set PATH so it include jdtls (prefers a custom build if present)
if [ -d "$HOME/git/eclipse.jdt.ls/org.eclipse.jdt.ls.product/target/repository/bin" ]; then
	PATH="$HOME/git/eclipse.jdt.ls/org.eclipse.jdt.ls.product/target/repository/bin:$PATH"
	export JDTLS_HOME=$HOME/git/eclipse.jdt.ls/org.eclipse.jdt.ls.product/target/repository/
elif [ -d "/opt/jdtls/bin" ]; then
	PATH="/opt/jdtls/bin:$PATH"
	export JDTLS_HOME=/opt/jdtls/
fi

# phpactor (php language server)
if [ -d "$HOME/git/phpactor/bin" ]; then
	PATH="$HOME/git/phpactor/bin:$PATH"
fi

# rust
export RUST_HOME=$HOME/.config/rust
export CARGO_HOME=$RUST_HOME/cargo
export RUSTUP_HOME=$RUST_HOME/rustup
if [ -d "$CARGO_HOME/bin" ]; then
	PATH="$CARGO_HOME/bin:$PATH"
fi

# lua language server
if [ -d "$HOME/git/lua-language-server/bin" ]; then
	PATH="$HOME/git/lua-language-server/bin:$PATH"
fi

# set PYTHONPATH so it includes the local repositories
if [ -d "$HOME/git/youtube-dl/youtube_dl" ]; then
	PYTHONPATH=$HOME/git/youtube-dl/youtube_dl:$PYTHONPATH
fi
export PYTHONPATH=/usr/local/lib/python3.10/site-packages:/usr/lib/python3/dist-packages:$PYTHONPATH

# set path to include executables from c projects
PATH="$HOME/git/pixls:$PATH"

# fzf
if [ -d "$HOME/git/fzf/bin" ]; then
	export PATH="$HOME/git/fzf/bin:$PATH"
fi

# java
export JAVA_HOME=/usr/lib/jvm/jdk-21-oracle-x64/
export JDK_HOME=/usr/lib/jvm/jdk-21-oracle-x64/
export PATH_TO_FX=/usr/lib/jvm/openjfx-11.0.2-sdk/lib

# yarn globals
PATH="$HOME/.yarn/bin:$PATH"

# composer globals
PATH="$HOME/.config/composer/vendor/bin:$PATH"

# other
export pw=$HOME/accounts/pw
export YGO_DB_LANGUAGE=de

# set neo-vim as default editor
export EDITOR='nvim'
export VISUAL='nvim'
export PAGER='less -MNWJx4'
export BROWSER='google-chrome'

# vim:ft=sh
