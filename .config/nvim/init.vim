" Plugins
call plug#begin('~/.config/nvim/plugged')
Plug 'ap/vim-css-color'
Plug 'https://gitlab.com/DrWursterich/vim-spml.git'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'cespare/vim-toml'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-context'
Plug 'nvim-treesitter/playground', {'do': ':TSUpdate'}
Plug 'JoosepAlviste/nvim-ts-context-commentstring'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzy-native.nvim', {'do': 'make'}
Plug 'nvim-telescope/telescope-ui-select.nvim'
Plug 'ThePrimeagen/refactoring.nvim'
Plug 'neovim/nvim-lspconfig'
Plug 'glepnir/lspsaga.nvim'
Plug 'jbyuki/venn.nvim'
Plug 'stephpy/vim-php-cs-fixer'
Plug '$HOME/git/sp-php-vim'
call plug#end()

" Functions
function! MapToggle(key, opt)
	let cmd = ':set '.a:opt.'! \| set '.a:opt."?\<CR>"
	exec 'nnoremap '.a:key.' '.cmd
endfunction
command -nargs=+ MapToggle call MapToggle(<f-args>)

function! ToggleCC()
	if &cc == ''
		set cc=81
		echom('colorcolumn  80')
	elseif &cc == 81
		set cc=101
		echom('colorcolumn 100')
	elseif &cc == 101
		set cc=121
		echom('colorcolumn 120')
	else
		set cc=
		echom('colorcolumn off')
	endif
endfunction

function! ToggleNumbers()
	if &number
		:set nonumber norelativenumber foldcolumn=0 nolist signcolumn=no mouse=
	else
		:set number relativenumber foldcolumn=3 list signcolumn=number mouse=nvi
	end
endfunction

function! ToggleJsonStyle()
	if &expandtab
		:set noexpandtab
	else
		:set expandtab
	end
	:retab
endfunction

function! FormatLog()
	% s/\\\\/\\/g
	% s/\\n/\r/g
	% s/:\ /:\r\ \ \ \ /g
	syn match Comment "\[.*\]"
	syn region Boolean start="SP\\" end=" in"ms=s-3 end="$"
	syn region File start="/" end=" " end="$" end="\.php"
	syn match Label "\(\d\+\)"
	syn match Label ":\d\+"ms=s+1
	syn match Label "line\s*\d\+"ms=s+4
	highlight File ctermfg=green
endfunction

function! FormatSiteparkJson()
	% s/\([{=,}\]\[]\)\ *\([^\ "{=,}\]\[][^"{=,}\]\[]*\)/\1"\2"/g
	% s/=/:/g
	% s/"\([^"(]*([^")]*\)"\ *:\ *"\([^"()]*[^"]*)\)"/"\1\2"/g
	% s/"\["\([^"]*\)"\]\"/[\1]/g
	% s/:\ *\([\]\},]\)/:null\1/g
	% !jq .
	set ft=json
endfunction

" formats whatever the clipboard contains after copying a want-list from
" cardmarket.com to valid bash syntax, assigning the variables 'amount',
" 'name' and 'sets' on one line per card.
function! FormatCardmarketList()
	" remove the unnecessary information
	% s/\(\:\?German\|Deutsch\)\n.\nNM\nNear Mint\nAny\tAny\tAny\tN\/A\tN\t\?\n//
	" remove set names
	% s/[\t\n]\([A-Z0-9]\{3,4\}\|Any\|Egal\)\(\:\?\t\|\n[^\t]\+\)/\r\1/
	" join set abbreviationins ','-separated
	% g/\(\:\?\(\:\?[A-Z0-9]\{3,4\}\|Any\|Egal\)\n\)\{2,\}/,/\(\:\?[A-Z0-9]\{3,4\}\|Any\|Egal\)\n\(\:\?[A-Z0-9]\{3,4\}\|Any\|Egal\)\@!/-1s/\n/,/
	" escape quotes
	% s/'/\\'/ge
	" transform the values to bash variables assignments
	% s/^\(\d\+\)\t\([^\t\n]\+\)[\t\n]\{1,2\}\([A-Z0-9,]\{3,\}\|Any\|Egal\)\n/amount=\1; name='\2'; sets='\3';/
	" sort everything by the name value
	sort /amount=\d\+; name='/
endfunction

function! SuperWrite(Q)
	silent write !sudo tee %
	edit!
	if a:Q == "true"
		quit
	endif
endfunction

function! ToggleTrimWhitespace()
	if exists('b:noTrimWhitespace')
		unlet b:noTrimWhitespace
	else
		let b:noTrimWhitespace=1
	endif
endfunction

function! TrimWhitespace()
	if exists('b:noTrimWhitespace')
		return
	endif
	let l:save = winsaveview()
	keeppatterns %s/\s\+$//e
	call winrestview(l:save)
endfunction

function ExecuteJava()
	let class = ''
	let start = 0
	for dir in split(expand('%:p:r'), '/')
		if dir == 'com' || dir == 'org' || dir == 'de'
			let start = 1
			let class = dir
		elseif start == 1
			let class .= '.' . dir
		endif
	endfor
	exec ':!mvn -B exec:java -Dexec.mainClass="' . class . '"'
endfunction

function! SymfonyClearCache()
	let path = './'
	for dir in reverse(split(expand('%:p:h'), '/'))
		if !empty(glob(path . 'bin/console'))
			exec('!rm -rf ' . path . 'var/cache/{dev,prod} && ' . path . 'bin/console cache:clear --no-ansi --no-optional-warmers')
			return
		else
			let path .= '../'
		endif
	endfor
	echomsg 'no bin/console executable found'
endfunction

function! MusicTitles()
	%s/^/\=printf('%02d ', line('.'))
	%s/$/\.mp3/
endfunction

function! SumSelection() range abort
	let sum = 0
	let [y1, x1] = getpos("'<")[1:2]
	let [y2, x2] = getpos("'>")[1:2]
	let lines = getline(y1, y2)
	if len(lines) > 0
		for line in lines
			let sum += str2float(substitute(trim(line[x1 - 1:x2 - 1]), ',', '.', 'g'))
		endfor
	endif
	echom(sum)
endfunction

:let mapleader = " "

" Mappings
MapToggle <F1> hlsearch
nnoremap <F2> :call ToggleCC()<CR>
nnoremap <F9> :call ToggleNumbers()<CR>
nnoremap <F10> :call ToggleTrimWhitespace()<CR>

" automaticaly toggle paste when inserting
" not quite sure how/why it works, but it does
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"
inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()
function! XTermPasteBegin()
	set pastetoggle=<Esc>[201~
	set paste
	return ""
endfunction

nnoremap <C-J> :resize +1<CR>
nnoremap <C-K> :resize -1<CR>
nnoremap <C-L> :vertical resize +1<CR>
nnoremap <C-H> :vertical resize -1<CR>

" add relative line movements to the jump list
nnoremap <expr> j (v:count > 1 ? "m'" . v:count : "") . 'j'
nnoremap <expr> k (v:count > 1 ? "m'" . v:count : "") . 'k'

" move lines up/down
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
inoremap <C-j> <C-O>:m .+1<CR><C-O>==
inoremap <C-k> <C-O>:m .-2<CR><C-O>==

" git
nnoremap <leader>gb :Git blame<CR>
nnoremap <leader>gh :diffget //3<CR>
nnoremap <leader>gl :diffget //2<CR>
nnoremap <leader>gs :G<CR>

" Telescope
nnoremap <leader>gs <CMD>Telescope git_status<CR>
nnoremap <leader>gc <CMD>Telescope git_bcommits<CR>
nnoremap <leader>ff <CMD>Telescope git_files<CR>
nnoremap <leader>fp <CMD>Telescope find_files<CR>
nnoremap <leader>fs <CMD>Telescope grep_string<CR>
nnoremap <leader>fg <CMD>Telescope live_grep<CR>

" open all buffers
nnoremap <leader>ba :ball<CR>
nnoremap <leader>vba :vert ball<CR>

" navigate buffers / quickfixlist
nnoremap <leader>n :n<CR>
nnoremap <leader>p :prev<CR>
nnoremap <leader>cn :cn<CR>
nnoremap <leader>cp :cprev<CR>

" tree sitter
nnoremap <leader>it :InspectTree<CR>

let mappings = "
	\ mode  mapping      function\n
	\ n     gd           definition\n
	\ n     gD           definition new window\n
	\ n     K            hover\n
	\ n     <C-P>        command history\n
	\\n
	\ n     <leader>vws  wrkspace_symbol\n
	\ n     <leader>vd   diagnostic\n
	\ n     <leader>vn   next diagnostic\n
	\ n     <leader>vp   prev diagnostic\n
	\ n     <leader>va   code action\n
	\ n     <leader>vrr  references\n
	\ n     <leader>vrn  rename\n
	\\n
	\ n     <leader>tr   run tests in current file\n
	\\n
	\ n     <leader>ef   switch bracket content between single- and multi-line\n
	\\n
	\ n     <leader>it   open tree-sitter tree window\n
	\\n
	\ n     <leader>gs   telescope git status\n
	\ n     <leader>gc   telescope git commits\n
	\ n     <leader>ff   telescope git files\n
	\ n     <leader>fp   telescope all files\n
	\ n     <leader>fs   telescope grep string\n
	\ n     <leader>fg   telescope live grep\n
	\\n
	\ v     <leader>rr   refactor prompt\n
	\ n     <leader>ri   inline variable\n
	\ v/n   <leader>rv   insert printf\n
	\ n     <leader>rc   cleanup"

nnoremap <leader>? :echo(mappings)<CR>

lua <<EOF

vim.o.termguicolors = true

local home = vim.fn.expand('$HOME')

-- key mapping shortcuts
local map = function(mode, keys, cmd)
	local opts = {
		noremap = true,
		silent = false,
		expr = false
	}
	if (type(cmd) == 'function') then
		opts['callback'] = cmd
		cmd = ''
	end
	vim.api.nvim_buf_set_keymap(0, mode, keys, cmd, opts)
end
local imap = function(keys, cmd)
	map('i', keys, cmd)
end
local nmap = function(keys, cmd)
	map('n', keys, cmd)
end
local vmap = function(keys, cmd)
	map('v', keys, cmd)
end

local on_attach = function(client, bufnr)
	nmap('gd', vim.lsp.buf.definition)
	nmap('gD', '<CMD>split | lua vim.lsp.buf.definition()<CR>')
	nmap('K', vim.lsp.buf.hover)
	nmap('<leader>vf', vim.lsp.buf.format)
	nmap('<leader>vws', vim.lsp.buf.workspace_symbol)
	nmap('<leader>vd', vim.diagnostic.open_float)
	nmap('<leader>vn', vim.diagnostic.goto_next)
	nmap('<leader>vp', vim.diagnostic.goto_prev)
	nmap('<leader>va', vim.lsp.buf.code_action)
	nmap('<leader>vrr', vim.lsp.buf.references)
	nmap('<leader>vrn', vim.lsp.buf.rename)
	imap('<C-k>', vim.lsp.buf.signature_help)
	-- let the lsp autocomplete <C-x> and <C-p>
	vim.api.nvim_buf_set_option(0, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
end

function toggle_java_style()
	if vim.o.expandtab then
		vim.o.expandtab = false
		vim.o.tabstop = 4
		vim.o.shiftwidth = 4
		vim.o.textwidth = 80
	else
		vim.o.expandtab = true
		vim.o.tabstop = 2
		vim.o.shiftwidth = 2
		vim.o.textwidth = 100
	end
	vim.cmd.retab()
end

function toggle_php_style()
	vim.o.expandtab = not vim.o.expandtab
	vim.cmd.retab()
end

function inline_get_model(buffer)
	buffer = buffer or 0 -- current buffer
	local Reader = require('sp_php_vim.reader')
	local Writer = require('sp_php_vim.writer')
	local start_row, start_col, end_row, end_col, return_type
	local ts_utils = require('nvim-treesitter.ts_utils')
	local node = ts_utils.get_node_at_cursor(nil, false)
	while (node:type() ~= 'method_declaration') do
		node = node:parent()
		if (node == nil) then
			error('cursor not inside a method declaration')
		end
	end
	local method_name = Reader.get_node_text(buffer, node:named_child(1))
	if (method_name ~= 'getModel') then
		error('cursor not inside a getModel method')
	end
	_, _, end_row, end_col = node:range()
	local sibling = node:prev_sibling()
	if (sibling:type() == 'comment') then
		start_row, start_col, _ = sibling:prev_sibling():end_()
		comment_start, _, _ = sibling:start()
		comment_end, _, _ = sibling:end_()
		local query = vim.treesitter.query.parse(
				'phpdoc',
				[[
					(tag
						(tag_name) @tag_name
						(#eq? @tag_name "@return")) @return
				]])
		for _, match, _ in query:iter_matches(
				vim.treesitter.get_parser(buffer, 'phpdoc'):parse()[1]:root(),
				buffer,
				comment_start,
				comment_end) do
			return_type = Reader.get_node_text(
					buffer,
					match[1]:parent():child(1))
		end
	else
		start_row, start_col, _ = sibling:end_()
	end
	Writer.set_text(
			buffer,
			{
				start_row,
				start_col,
				end_row,
				end_col,
			},
			{})
	if (return_type == nil) then
		return
	end
	local class_query = vim.treesitter.query.parse(
			'php',
			[[
				(class_declaration) @class
			]])
	for _, class, _ in class_query:iter_captures(
			vim.treesitter.get_parser(buffer, 'php'):parse()[1]:root(),
			buffer,
			0,
			vim.api.nvim_buf_line_count(buffer)) do
		class_comment = class:prev_sibling()
		if (class_comment:type() == 'comment') then
			local line, _, _ = class_comment:end_()
			Writer.insert_snippet_at(
					buffer,
					line,
					{
						' * @method ' .. return_type .. ' getModel()',
					})
		else
			local line, _, _ = class:start()
			Writer.insert_snippet_at(
					buffer,
					line,
					{
						'/**',
						' * @method ' .. return_type .. ' getModel()',
						' */',
					})
		end
		break
	end
end

function setup_sp_php_vim()
	package.loaded['sp_php_vim'] = nil
	package.loaded['sp_php_vim.config'] = nil
	require('sp_php_vim').setup({
		snippets = {
			initModel = {
				key_binding = '<leader>rim',
			},
			initComponent = {
				key_binding = '<leader>ric',
			},
			processComponent = {
				key_binding = '<leader>rpc',
			},
			finishComponent = {
				key_binding = '<leader>rfc',
			},
			initRender = {
				key_binding = '<leader>rir',
			},
			render = {
				key_binding = '<leader>rr',
			},
			property = {
				key_binding = '<leader>rp',
			},
			getter = {
				key_binding = '<leader>rg',
			},
			setter = {
				key_binding = '<leader>rs',
			},
			hasser = {
				key_binding = '<leader>rh',
			},
			isser = {
				key_binding = '<leader>ri',
			},
			optional_isser = {
				key_binding = '<leader>roi',
			},
			model_property = {
				key_binding = '<leader>rmp',
			},
			handleResolvedInternal = {
				key_binding = '<leader>rhri',
			},
			handleBeforeResolveInternal = {
				key_binding = '<leader>rhbri',
			},
			handleResolvedExternal = {
				key_binding = '<leader>rhre',
			},
			handleBeforeResolveExternal = {
				key_binding = '<leader>rhbre',
			},
		},
	})
	nmap('<leader>r<Space>', require('telescope').extensions.sp_php_vim.snippets)
	nmap('<leader>ru', require('sp_php_vim.snippet.importer').shorten_type_under_cursor)
	nmap('<leader>rgm', inline_get_model)
end

function toggle_php_arguments()
	local is_new_format = vim.o.expandtab
	local buffer = 0
	local language = 'php'
	-- make sure tree is parsed before invoking get_node
	vim.treesitter.get_parser(buffer, language):parse()
	local node = vim.treesitter.get_node({bufnr = buffer})
	local type = nil
	while true do
		type = node:type()
		if type == "formal_parameters" or type == "arguments" or type == "array_creation_expression" then
			break
		end
		node = node:parent()
		if node == nil then
			print("coursor not in appropriate brackets")
			return
		end
	end
	local result = {}
	local start_row, start_col, end_row, end_col = node:range()
	local line = vim.api.nvim_buf_get_lines(buffer, start_row, start_row + 1, true)[1]
	local indent = string.gsub(line, "[^ \t].*$", "")
	local next_indent = indent .. "\t"
	if is_new_format then
		next_indent = indent .. "    "
	end
	if start_row == end_row then
		for child in node:iter_children() do
			local child_type = child:type()
			if child_type == "(" then
				table.insert(result, "(")
			elseif child_type == ")" then
				local line = indent .. ")"
				-- also transform function declaration brackets
				if is_new_format and type == "formal_parameters" then
					node = node:next_sibling()
					if node:type() == ":" then
						node = node:next_sibling()
						line = line .. ": " .. vim.treesitter.get_node_text(node, buffer, {})
						node = node:next_sibling()
					end
					if node:type() == "compound_statement" then
						node = node:child(0)
						end_row, end_col, _ = node:end_()
						line = line .. " " .. vim.treesitter.get_node_text(node, buffer, {})
					end
				end
				table.insert(result, line)
			elseif child_type == "[" then
				table.insert(result, "[")
			elseif child_type == "]" then
				table.insert(result, indent .. "]")
			elseif child_type ~= "," then
				local node_content = vim.treesitter.get_node_text(child, buffer, {})
				table.insert(result, next_indent .. node_content .. ",")
			end
		end
	else
		local text = ''
		for child in node:iter_children() do
			local child_type = child:type()
			if child_type == "(" then
				text = "("
			elseif child_type == ")" then
				text = text .. ")"
				-- also transform function declaration brackets
				if is_new_format and type == "formal_parameters" then
					node = node:next_sibling()
					if node:type() == ":" then
						node = node:next_sibling()
						text = text .. ": " .. vim.treesitter.get_node_text(node, buffer, {})
						node = node:next_sibling()
					end
					result = {text}
					if node:type() == "compound_statement" then
						node = node:child(0)
						end_row, end_col, _ = node:end_()
						local node_content = vim.treesitter.get_node_text(node, buffer, {})
						table.insert(result, indent .. node_content)
					end
				else
					result = {text}
				end
			elseif child_type == "[" then
				text = "["
			elseif child_type == "]" then
				text = text .. "]"
				result = {text}
			elseif child_type ~= "," then
				if text:len() > 1 then
					text = text .. ", "
				end
				local node_content = vim.treesitter.get_node_text(child, buffer, {})
				text = text .. node_content
			end
		end
	end
	vim.api.nvim_buf_set_text(buffer, start_row, start_col, end_row, end_col, result)
	vim.api.nvim_win_set_cursor(buffer, { start_row + 1, start_col })
end

function toggle_java_arguments()
	local is_new_format = vim.o.expandtab
	local buffer = 0
	local language = 'java'
	-- make sure tree is parsed before invoking get_node
	vim.treesitter.get_parser(buffer, language):parse()
	local node = vim.treesitter.get_node({bufnr = buffer})
	while true do
		local type = node:type()
		if type == "argument_list" or type == "formal_parameters" then
			break
		end
		node = node:parent()
		if node == nil then
			print("coursor not in appropriate brackets")
			return
		end
	end
	local result = {}
	local start_row, start_col, end_row, end_col = node:range()
	local text = ''
	if start_row == end_row then
		local line = vim.api.nvim_buf_get_lines(buffer, start_row, start_row + 1, true)[1]
		local indent = string.gsub(line, "[^ \t].*$", "")
		local next_indent = indent .. "\t\t"
		if is_new_format then
			next_indent = indent .. "    "
		end
		for child in node:iter_children() do
			local child_type = child:type()
			if child_type == "(" then
				table.insert(result, "(")
			elseif child_type == ")" then
				table.insert(result, text .. ")")
			elseif child_type ~= "," then
				if text:len() > 1 then
					table.insert(result, text .. ",")
				end
				text = next_indent .. vim.treesitter.get_node_text(child, buffer, {})
			end
		end
	else
		for child in node:iter_children() do
			local child_type = child:type()
			if child_type == "(" then
				text = "("
			elseif child_type == ")" then
				text = text .. ")"
			elseif child_type ~= "," then
				if text:len() > 1 then
					text = text .. ", "
				end
				text = text .. vim.treesitter.get_node_text(child, buffer, {})
			end
		end
		result = {text}
	end
	vim.api.nvim_buf_set_text(buffer, start_row, start_col, end_row, end_col, result)
	vim.api.nvim_win_set_cursor(buffer, { start_row + 1, start_col })
end

function execute_maven_tests()
	local mvn_test_namespace = vim.api.nvim_create_namespace('mvn_test')
	local bufnr = vim.api.nvim_get_current_buf()
	local root = vim.lsp.buf_get_clients()[1].config.root_dir
	local class, _ = vim.fn.expand("%:p"):gsub(".java$", "")
	class, _ = class:gsub("^.*/test/java/", "")
	class, _ = class:gsub("/", ".")
	local cmd = 'mvn test -B -q -f ' .. root .. ' -Dtest=' .. class
	local failures = {}
	local summary = nil
	vim.api.nvim_buf_clear_namespace(bufnr, mvn_test_namespace, 0, -1)
	vim.fn.jobstart(cmd, {
		stdout_buffered = true,
		on_stdout = function(_, data)
			if not data then
				return
			end
			local state = 'start'
			local method = nil
			local message = nil
			local line_nr = nil
			for _, line in ipairs(data) do
				print('> ' .. line)
				if (line:find('.*%bTests run: %d+, Failures: %d+, Errors: %d+, Skipped: %d+$')) then
					summary = line
					return
				elseif (line:find(' <<< FAILURE!$')) then
					if (state == 'start') then
						state = 'failures'
					elseif (state == 'failures') then
						-- method = line:gsub('^[^(]+%.([^(]+)%(%).*$', '%1')
						-- method = line:gsub('^.* ([^ ]+) %-%- .*$', '%1')
						method = line:gsub('^[^a-z]*([^ ]+).*$', '%1')
						state = 'message'
					end
				-- elseif (line:find('^%s+at ') and (state == 'message' or state == 'trace')) then
				elseif (line:find('%bat ') and (state == 'message' or state == 'trace')) then
					state = 'trace'
					-- line_nr = line:gsub('^.*%.java:(%d+)%)$', '%1')
					local trace_line_nr = line:gsub('^.*%bat ' .. method .. '%(.*%.java:(%d+)%)$', '%1')
					print('method: ' .. method .. ' | trace_line_nr: ' .. trace_line_nr)
					trace_line_nr = tonumber(trace_line_nr, 10)
					if trace_line_nr then
						line_nr = trace_line_nr
					end
				elseif (state == 'message') then
					if not message then
						message = line:gsub('^org%.opentest4j%.AssertionFailedError: ', 'test failed: ')
					else
						message = message .. '\n' .. line
					end
				elseif (state == 'trace' and line == '') then
					table.insert(failures, {
						bufnr = bufnr,
						lnum = line_nr - 1,
						col = vim.fn.getline(line_nr):find("%S") - 1,
						severity = vim.diagnostic.severity.ERROR,
						source = 'mvn test',
						message = message,
						user_data = {
							method = method,
						},
					})
					state = 'failures'
					message = nil
				end
			end
		end,
		on_exit = function()
			vim.diagnostic.set(mvn_test_namespace, bufnr, failures, {})
			if summary then
				print(summary)
			else
				print('all test completed successfully!')
			end
		end
	})
end

-- PHP
local php_group = vim.api.nvim_create_augroup('php', { clear = true })
vim.api.nvim_create_autocmd({ 'FileType' }, {
	pattern = { 'php' },
	group = php_group,
	callback = function(event)
		vim.o.textwidth = 80
		setup_sp_php_vim()
		-- vim.g.php_cs_fixer_path = home .. '/.config/composer/vendor/bin/php-cs-fixer'
		-- vim.g.php_cs_fixer_config_file = home .. '/.config.nvim/config.php_cs'
		-- vim.g.php_cs_fixer_config_cache = home .. '/.vim/swapfiles/.php-cs.cache'
		-- vim.g.php_cs_fixer_config_allow_risky = 'yes'
		nmap('<F5>', [[<CMD>call SymfonyClearCache()<CR>]])
		-- nmap('<F7>', [[<CMD>call PhpCsFixerFixFile<CR>]])
		nmap('<F8>', toggle_php_style)
		nmap('<leader>ef', toggle_php_arguments)
	end,
})

-- JAVA
local java_group = vim.api.nvim_create_augroup('java', { clear = true })
vim.api.nvim_create_autocmd({ 'FileType' }, {
	pattern = { 'java' },
	group = java_group,
	callback = function(event)
		vim.o.textwidth = 100
		nmap('<F5>', [[<CMD>call ExecuteJava()<CR>]])
		nmap('<F7>', [[<CMD>%!java -jar /opt/google/google-java-format-1.19.2-all-deps.jar %<CR>]])
		nmap('<F8>', toggle_java_style)
		nmap('<leader>ef', toggle_java_arguments)
		nmap('<leader>tr', execute_maven_tests)
	end,
})

-- SPML
local spml_group = vim.api.nvim_create_augroup('spml', { clear = true })
local lspml = home .. '/git/lspml'
vim.api.nvim_create_autocmd({ 'FileType' }, {
	pattern = { 'spml' },
	group = spml_group,
	callback = function(event)
		vim.lsp.set_log_level('debug')
		-- manually attach lspml ls
		vim.lsp.start({
			name = 'lspml',
			cmd = {
				lspml .. '/target/debug/lspml',
				'--log-file',
				lspml .. '/lspml.log.json',
				'--log-level',
				'TRACE',
				'--modules-file',
				lspml .. '/module_mappings.json',
			},
			root_dir = vim.fs.dirname(vim.fs.find({ 'src' }, { upward = true })[1]),
		})
	end
})
vim.api.nvim_create_autocmd({ 'LspAttach' }, {
	pattern = { '*.spml' },
	group = spml_group,
	callback = function(event)
		on_attach(event.data, event.buf)
	end
})

-- Refactoring
require('refactoring').setup({
	print_var_statements = {
		php = {
			'echo \'%s\'; // __AUTO_GENERATED_PRINT_VAR__\n        \\SP\\Util\\Debug::dump(%s);',
		}
	}
})
-- Remaps for the refactoring operations currently offered by the plugin
vmap('<leader>rr', [[<CMD>lua require('telescope').extensions.refactoring.refactors()<CR>]])
-- Inline variable can also pick up the identifier currently under the cursor without visual mode
nmap('<leader>ri', [[<CMD>lua require('refactoring').refactor('Inline Variable')<CR>]])
-- Remap in normal mode and passing { normal = true } will automatically find the variable under the cursor and print it
nmap('<leader>rv', [[<CMD>lua require('refactoring').debug.print_var({ normal = true })<CR>]])
-- Remap in visual mode will print whatever is in the visual selection
vmap('<leader>rv', [[<CMD>lua require('refactoring').debug.print_var({})<CR>]])
-- Cleanup function: this remap should be made in normal mode
nmap('<leader>rc', [[<CMD>lua require('refactoring').debug.cleanup({})<CR>]])

require('nvim-treesitter.configs').setup({
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false
	},
	incremental_selection = {
		enable = true
	},
	textobjects = {
		enable = true
	},
	indent = {
		-- breaks php indenting otherwise
		enable = true
	}
})

local telescope = require('telescope');
local telescope_previewers = require('telescope.previewers');
local telescope_actions = require('telescope.actions');
telescope.setup({
	defaults = {
		file_ignore_patterns = {
			'*-styleguide',
			'node_modules',
			'\\.git',
			'target',
			'vendor',
			'bin',
			'build',
		},
		file_sorter = require('telescope.sorters').get_fzy_sorter,
		prompt_prefix = ' > ',
		color_devicons = true,
		file_previewer = telescope_previewers.vim_buffer_cat.new,
		grep_previewer = telescope_previewers.vim_buffer_vimgrep.new,
		mappings = {
			i = {
				['<ESC>'] = telescope_actions.close,
				['<C-f>'] = telescope_actions.preview_scrolling_up,
				['<C-b>'] = telescope_actions.preview_scrolling_down,
			},
		},
		vimgrep_arguments = {
			'rg',
			'--no-config',
			'--color=never',
			'--no-heading',
			'--with-filename',
			'--line-number',
			'--column',
			'--no-trim',
			'--ignore',
			'--case-sensitive'
		}
	},
	pickers = {
		command_history = {
			mappings = {
				i = {
					['<C-j>'] = telescope_actions.set_command_line,
				},
			},
		},
	},
	extensions = {
		fzy_native = {
			override_generic_sorter = false,
			override_file_sorter = true
		},
		['ui-select'] = {
			layout_strategy = 'cursor',
			layout_config = {
				height = 0.2,
				width = 0.6,
			},
		},
	},
})
telescope.load_extension('fzy_native')
telescope.load_extension('ui-select')
nmap('<C-p>', require('telescope.builtin').command_history)

require('lspconfig').clangd.setup({ on_attach = on_attach })
require('lspconfig').jdtls.setup({
	on_attach = on_attach,
	settings = {
		java = {
			configurations = {
				runtimes = {
					name = 'JavaSE-21',
					path = '/usr/lib/jvm/java-21-openjdk-amd64',
					default = true,
				},
			},
		},
	},
})
require('lspconfig').rust_analyzer.setup({
	cmd = {
		'rustup',
		'run',
		'nightly',
		--'stable',
		'rust-analyzer',
	},
	on_attach = on_attach,
	settings = {
		['rust-analyzer'] = {
			diagnostics = {
				enable = true
			}
		}
	},
	init_options = {
	}
})
require('lspconfig').phpactor.setup({
	on_attach = on_attach,
	init_options = {
		['language_server.diagnostics_on_update'] = true,
		['language_server.diagnostic_sleep_time'] = 500,
		['language_server_configuration.auto_config'] = false,
		['language_server_phpstan.enabled'] = true,
		-- ['language_server_phpstan.bin'] = '%project_root%/tools/phpstan',
		['language_server_phpstan.bin'] = '~/.config/composer/vendor/bin/phpstan',
		-- ['language_server_phpstan.bin'] = '~/.config/nvim/phpstan_with_config',
		['language_server_phpstan.level'] = '9',
		['language_server_psalm.enabled'] = true,
		['language_server_psalm.bin'] = '~/.config/composer/vendor/bin/psalm',
		['language_server_php_cs_fixer.enabled'] = true,
		['language_server_php_cs_fixer.bin'] = '~/.config/composer/vendor/bin/php-cs-fixer',
		-- phpcs has no ruleset for per-cs2.0, php-cs-fixer does
		-- ['php_code_sniffer.enabled'] = true,
		-- ['php_code_sniffer.bin'] = '~/.config/composer/vendor/bin/phpcs',
		['prophecy.enabled'] = true,
		['symfony.enabled'] = true,
		['phpunit.enabled'] = true,
		['language_server_indexer.workspace_symbol_search_limit'] = 4000,
	}
})

local create_venn_bindings = function()
	-- draw a line on HJKL keystokes
	nmap('J', '<C-v>j:' .. vim.b.venn_style .. '<CR>')
	nmap('K', '<C-v>k:' .. vim.b.venn_style .. '<CR>')
	nmap('L', '<C-v>l:' .. vim.b.venn_style .. '<CR>')
	nmap('H', '<C-v>h:' .. vim.b.venn_style .. '<CR>')
	-- draw a box by pressing 'f' with visual selection
	vmap('f', ':' .. vim.b.venn_style .. '<CR>')
	-- fill a visual selection
	vmap('o', ':VFill<CR>')
end
vim.b.venn_style = 'VBox'
function toggle_venn_style()
	local venn_style = vim.b.venn_style
	if venn_style == 'VBox' then
		print('venn style: bold');
		vim.b.venn_style = 'VBoxH'
	elseif venn_style == 'VBoxH' then
		print('venn style: outline');
		vim.b.venn_style = 'VBoxD'
	else
		print('venn style: normal');
		vim.b.venn_style = 'VBox'
	end
	if vim.b.venn_enabled then
		create_venn_bindings()
	end
end
function toggle_venn()
	if vim.b.venn_enabled == nil then
		vim.b.venn_enabled = true
		vim.cmd('setlocal virtualedit=all')
		create_venn_bindings()
	else
		vim.cmd('setlocal virtualedit=')
		vim.cmd('mapclear <buffer>')
		vim.b.venn_enabled = nil
	end
end
nmap('<leader>dr', toggle_venn)
nmap('<leader>ds', toggle_venn_style)

require('ts_context_commentstring').setup({
	languages = {
		java = { __default = '// %s', __multiline = '/* %s */' },
		spml = '<%-- %s --%>',
	}
})
-- disable backwards compatibility
vim.g.skip_ts_context_commentstring_module = true

EOF

autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | silent! pclose | endif

filetype plugin indent on

nnoremap Y yg_

" sort paragraph lines alphabeticaly
nnoremap <leader>s Vip:sort<CR>

autocmd BufWritePre * call TrimWhitespace()

command FormatLog call FormatLog()
command FormatSiteparkJson call FormatSiteparkJson()
command FormatCardmarketList call FormatCardmarketList()
command MusicTitles call MusicTitles()
command SumSelection call SumSelection()
command W call SuperWrite("false")
command WQ call SuperWrite("true")
command Date .-1r!date +\%d.\%m.\%g

" reserve <C-r> mappings, that would be overwritten otherwise
cnoremap <C-r>" <C-r>"
cnoremap <C-r>% <C-r>%
cnoremap <C-r># <C-r>#
cnoremap <C-r>* <C-r>*
cnoremap <C-r>+ <C-r>+
cnoremap <C-r>/ <C-r>/
cnoremap <C-r>\: <C-r>\:
cnoremap <C-r>- <C-r>-
cnoremap <C-r>. <C-r>.
cnoremap <C-r>= <C-r>=
cnoremap <C-r>0 <C-r>0
cnoremap <C-r>1 <C-r>1
cnoremap <C-r>2 <C-r>2
cnoremap <C-r>3 <C-r>3
cnoremap <C-r>4 <C-r>4
cnoremap <C-r>5 <C-r>5
cnoremap <C-r>6 <C-r>6
cnoremap <C-r>7 <C-r>7
cnoremap <C-r>8 <C-r>8
cnoremap <C-r>9 <C-r>9

" command mode mappings to be bash-like
cnoremap <C-a> <C-b>
cnoremap <ESC><BS> <C-w>
cnoremap <C-p> <Up>
cnoremap <C-n> <Down>
cnoremap <C-j> <CR>
cnoremap <C-r> <C-f>?
cnoremap <C-l> <C-d>
cnoremap <C-d> <Del>
cnoremap <C-b> <Left>
cnoremap <C-f> <Right>
cnoremap <ESC>b <C-\>eCmdMoveBackWord()<CR>
cnoremap <ESC>f <C-\>eCmdMoveForwardWord()<CR>

function CmdMoveBackWord()
	let cmd = getcmdline()
	let split_pos = getcmdpos() - 2
	if split_pos <= 1
		call setcmdpos(1)
		return cmd
	endif
	let left = cmd[:split_pos]
	let pos = strlen(split(left, '\>[^$]\(.\+\>[^$]\)\@!\zs', 1)[0])
	call setcmdpos(pos)
	return cmd
endfunction

function CmdMoveForwardWord()
	let cmd = getcmdline()
	let pos = getcmdpos()
	let right = cmd[pos:]
	let offset = strlen(split(right, '[\>\/\\\|\.:_,;\-+]\zs', 1)[0])
	call setcmdpos(pos + offset)
	return cmd
endfunction

" SPML, HTML, XML, XSL, XSLT
augroup markup_mappings
	autocmd!
	autocmd FileType spml,html,xml,xsl,xslt nmap <expr> <leader>< 'a<' . input('Tag-Name: ') . ' >\<Esc>T<yt f>a</>\<Esc>hpF>dh'
	autocmd FileType spml,html,xml,xsl,xslt nmap <expr> <leader>> 'a<' . input('Tag-Name: ') . '/>\<Esc>'
	autocmd FileType spml,html,xml,xsl,xslt nmap <expr> <leader><Space> 'a\<CR>\<CR>\<Esc>kA\<Tab>\<Esc>'
	autocmd FileType spml,html,xml,xsl,xslt nnoremap <expr> <cr> getline('.')[col('.') - 1:col('.')] == '><' ? 'a<cr><c-o>O': '<cr>'
	autocmd FileType spml,html,xml,xsl,xslt nnoremap <F7> :%!xmlstarlet format --indent-tab<CR>
	autocmd FileType spml,html,xml,xsl,xslt vnoremap <F7> :'<,'>!xmlstarlet format --indent-tab<CR>
augroup END
augroup spml_mappings
	autocmd!
	autocmd BufNewFile *.spml 0r $HOME/.vim/templates/skeleton.spml
	autocmd FileType spml nmap <expr> <leader>/ "a<%--  --%>\<Esc>5h"
	autocmd FileType spml setlocal commentstring=<%--\ %s\ --%>
	" currently required for the lsp's autocomplete
	autocmd FileType spml set iskeyword+=:
augroup END

" Rmarkdown
augroup rmarkdown_mappings
	autocmd!
	autocmd FileType markdown,rmd map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter>
	autocmd FileType markdown,rmd let b:noTrimWhitespace=1
augroup END

" Git diffs
augroup diff
	autocmd!
	autocmd FileType diff let b:noTrimWhitespace=1
augroup END

augroup json_mappings
	autocmd!
	autocmd FileType json nnoremap <F7> :%!jq . --tab<CR>
	autocmd FileType json vnoremap <F7> :'<,'>!jq . --tab<CR>
	autocmd FileType json set noexpandtab
	autocmd FileType json nnoremap <F8> :call ToggleJsonStyle()<CR>
augroup END

augroup dot_mappings
	autocmd!
	autocmd FileType dot nnoremap <F5> :exec '!dot -Tpng ' . expand('%:p') . ' > ' . expand('%:p:r') . '.png'<CR>
augroup END

augroup lock_mappings
	autocmd!
	autocmd BufRead *.lock set ft=json
augroup END

augroup rust
	autocmd!
	autocmd FileType rust nnoremap <F5> :!cargo run<CR>
	autocmd FileType rust nnoremap <F7> :!rustfmt --edition 2021 %<CR>
	autocmd FileType rust set textwidth=100
	autocmd FileType rust set iskeyword-=:
	autocmd FileType rust set iskeyword+=&
augroup END

augroup nasm
	autocmd!
	autocmd FileType nasm set ft=nasm
augroup END

augroup xxd
	autocmd!
	autocmd FileType xxd set ft=xxd
augroup END

augroup lsp_document_highlight
	autocmd!
	autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
	autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
augroup END

" Highlighting
colorscheme sorbet

augroup highlights
	autocmd ColorScheme * highlight Normal guibg=clear
	autocmd ColorScheme * highlight @lsp.mod.deprecated gui=strikethrough
	autocmd ColorScheme * highlight @lsp.type.number guifg=red
	autocmd ColorScheme * highlight @lsp.type.operator guifg=#5495EA
	autocmd ColorScheme * highlight LspReferenceRead gui=bold
	autocmd ColorScheme * highlight LspReferenceText gui=bold
	autocmd ColorScheme * highlight LspReferenceWrite gui=bold
	autocmd ColorScheme sorbet highlight ColorColumn guibg=#161616
	autocmd ColorScheme sorbet highlight NormalFloat guibg=#161821
	autocmd ColorScheme sorbet highlight DiagnosticError guifg=red
	autocmd ColorScheme sorbet highlight @diff.minus guifg=red
	autocmd ColorScheme sorbet highlight @diff.plus guifg=lightgreen
	autocmd ColorScheme * highlight default link @lsp.type.regexp @string.regexp
augroup END

" highlight Normal guibg=clear
" highlight FoldColumn ctermbg=black ctermfg=white
" highlight Fold ctermfg=black
" highlight StatusLine ctermfg=darkgrey ctermbg=white
" highlight Visual ctermfg=black
" highlight Search ctermfg=black
" highlight DiffAdd ctermbg=4
" highlight DiffChange ctermbg=5
" highlight DiffText ctermbg=173
" highlight CursorLineNr ctermfg=white
" highlight NonText ctermfg=16 guifg=black guibg=clear
" highlight WinSeparator guibg=None
" highlight DiagnosticWarn ctermfg=black
" highlight SpellBad ctermbg=224 ctermfg=0 gui=undercurl guisp=Red

let $BASH_ENV="~/.vim_env"

set nocompatible

set number
set relativenumber

set noexpandtab
set tabstop=4
set shiftwidth=4
set copyindent
set nopreserveindent
set autoindent
set smarttab
set scrolloff=6

set hlsearch
set incsearch

set nrformats-=octal
set formatoptions+=Bj

" options for the % operation
" may be better of replaced by vim-match-up plugin
set matchpairs+=<:>
packadd! matchit

set title
set splitbelow
set splitright
set shortmess=AFIacs
set foldcolumn=3
set signcolumn=number
set path=**
set wildmenu
set wildignore+=**/.git/*
set wildignore+=**/target/*
set wildignore+=**/node_modules/*
set wildignore+=**/vendor/*
set wildmode=longest:full
set cmdheight=2

set updatetime=50

set laststatus=3
set statusline=%<%f\ %h%m%r%{FugitiveStatusline()}%=%-14.(%l,%c%V%)\ %P

set nofixendofline
set noerrorbells
set directory=$HOME/.vim/swapfiles/

" show whitespace characters
set list
set listchars=eol:\ ,tab:\|->,trail:~,extends:>,precedes:<

" otherwise highlighting breaks
set background=light

syntax on

