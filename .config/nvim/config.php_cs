<?php declare(strict_types=1);

$config = new PhpCsFixer\Config();
$config->setIndent("\t");
$config->setRules(
	[
		'@PSR12' => true,
		'braces' => [
			'allow_single_line_anonymous_class_with_empty_body' => true,
			'allow_single_line_closure' => true,
			'position_after_functions_and_oop_constructs' => 'same',
			'position_after_control_structures' => 'same',
			'position_after_anonymous_constructs' => 'same'
		],
		'blank_line_after_opening_tag' => false,
		'no_blank_lines_after_class_opening' => false,
		'no_leading_import_slash' => false,
		'single_blank_line_before_namespace' => false,
		'class_attributes_separation' => true,
		'no_null_property_initialization' => true,
		'date_time_immutable' => true,
		'align_multiline_comment' => true,
		'comment_to_phpdoc' => true,
		'multiline_comment_opening_closing' => true,
		'no_empty_comment' => true,
		'no_trailing_whitespace_in_comment' => true,
		'single_line_comment_spacing' => true
	]
);
return $config;
