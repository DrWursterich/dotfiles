# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

# This defines which key super maps to on your keyboard.
# Alt key is Mod1, and Windows key is Mod4
set $mod Mod4
set $alt Mod1

# i3xrocks config file
set $i3xrocks_config $HOME/.config/regolith/i3xrocks/config

# compton config file
set $compton_config $HOME/.config/regolith/compton/config

# path to terminal. Override this to specify a custom terminal.
set $terminal_path st

# focus rules
no_focus [window_role="pop-up"]
no_focus [floating]

# scratchpad
bindsym $mod+x move scratchpad
bindsym $mod+Ctrl+x scratchpad show

# Color values are defined in $HOME/.Xresources-regolith
# These are the labels which define each i3 workspace.
set_from_resource $ws1  i3-wm.workspace.01.name "1"
set_from_resource $ws2  i3-wm.workspace.02.name "2"
set_from_resource $ws3  i3-wm.workspace.03.name "3"
set_from_resource $ws4  i3-wm.workspace.04.name "4"
set_from_resource $ws5  i3-wm.workspace.05.name "5"
set_from_resource $ws6  i3-wm.workspace.06.name "6"
set_from_resource $ws7  i3-wm.workspace.07.name "7"
set_from_resource $ws8  i3-wm.workspace.08.name "8"
set_from_resource $ws9  i3-wm.workspace.09.name "9"
set_from_resource $ws10 i3-wm.workspace.10.name "10"
set_from_resource $ws11 i3-wm.workspace.11.name "11"
set_from_resource $ws12 i3-wm.workspace.12.name "12"
set_from_resource $ws13 i3-wm.workspace.13.name "13"
set_from_resource $ws14 i3-wm.workspace.14.name "14"
set_from_resource $ws15 i3-wm.workspace.15.name "15"
set_from_resource $ws16 i3-wm.workspace.16.name "16"
set_from_resource $ws17 i3-wm.workspace.17.name "17"
set_from_resource $ws18 i3-wm.workspace.18.name "18"
set_from_resource $ws19 i3-wm.workspace.19.name "19"

# use these keys for focus, movement, and resize directions when reaching for
# the arrows is not convenient
set $up k
set $down j
set $left h
set $right l

# use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

hide_edge_borders both

# start a terminal
bindsym $mod+Return exec --no-startup-id $terminal_path

# start google-chrome
bindsym $mod+Shift+i exec --no-startup-id chromium-browser
bindsym $mod+Shift+Ctrl+i exec --no-startup-id chromium-browser --incognito

# start mattermost
bindsym $mod+Shift+n exec --no-startup-id mattermost-desktop
# start team speak3
#bindsym $mod+Shift+n exec --no-startup-id team-speak

# start ranger
bindsym $mod+Shift+r exec --no-startup-id st -n "ranger" -e ranger $HOME

# start vlc
bindsym $mod+Shift+m exec --no-startup-id vlc --audio --started-from-file --playlist-enqueue --random --loop --playlist-tree --recursive="expand" $(ls -d $HOME/Music/.playlists/* | labeled_dmenu -i -l 20 -fn Monospace-16 --function "grep -Po '^(.*\/)?\K[^\.]*'" || echo $HOME/Music)

# vlc bindings
set $mpris_control $HOME/scripts/mpris_control
bindsym $mod+Shift+p            exec $mpris_control vlc pause
bindsym $mod+greater            exec $mpris_control vlc next
bindsym $mod+Shift+greater      exec $mpris_control vlc previous
bindsym $mod+Ctrl+greater       exec $mpris_control vlc increase_volume
bindsym $mod+Shift+Ctrl+greater exec $mpris_control vlc decrease_volume

# copy emojis
bindsym $mod+Shift+apostrophe exec $HOME/scripts/dmenuunicode

# launch quick_ssh
bindsym $mod+Shift+s exec st -n "quick_ssh" -e $HOME/scripts/quick_ssh
# start steam
bindsym $mod+Shift+s exec --no-startup-id steam

# start eclipse
bindsym $mod+Shift+o exec --no-startup-id eclipse

# start thunderbird
bindsym $mod+Shift+t exec --no-startup-id thunderbird

# screenshot with selection
bindsym --release $mod+Print exec --no-startup-id flameshot gui

# kill focused window
bindsym $mod+Shift+q kill

set_from_resource $rofiTheme rofi.theme "regolith-theme"

# rofi app and window launchers
bindsym $mod+space       exec --no-startup-id rofi -scroll-method 1 -show drun   -theme $rofiTheme
bindsym $mod+Shift+space exec --no-startup-id rofi -scroll-method 1 -show run    -theme $rofiTheme
bindsym $mod+Ctrl+space  exec --no-startup-id rofi -scroll-method 1 -show window -theme $rofiTheme

# change focus
bindsym $mod+$left  focus left
bindsym $mod+$down  focus down
bindsym $mod+$up    focus up
bindsym $mod+$right focus right

# move focused window
bindsym $mod+Shift+$left  move left
bindsym $mod+Shift+$down  move down
bindsym $mod+Shift+$up    move up
bindsym $mod+Shift+$right move right

# alternatively, you can use the cursor keys:
bindsym $mod+Left  focus left
bindsym $mod+Down  focus down
bindsym $mod+Up    focus up
bindsym $mod+Right focus right

bindsym $mod+Shift+Left  move left
bindsym $mod+Shift+Down  move down
bindsym $mod+Shift+Up    move up
bindsym $mod+Shift+Right move right

# toggle split orientation
bindsym $mod+BackSpace split toggle

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change gaps interactively
bindsym $mod+minus gaps inner current minus 6
bindsym $mod+plus  gaps inner current plus 6

# toggle tiling / floating
bindsym $mod+Shift+d floating toggle

# change focus between tiling / floating windows
bindsym $mod+d focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
bindsym $mod+q focus child

# change container layout (stacked, tabbed, default, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout default
bindsym $mod+t layout toggle splitv splith

# switch to workspace
bindsym $mod+1      workspace $ws1
bindsym $mod+2      workspace $ws2
bindsym $mod+3      workspace $ws3
bindsym $mod+4      workspace $ws4
bindsym $mod+5      workspace $ws5
bindsym $mod+6      workspace $ws6
bindsym $mod+7      workspace $ws7
bindsym $mod+8      workspace $ws8
bindsym $mod+9      workspace $ws9
bindsym $mod+0      workspace $ws10
bindsym $mod+Ctrl+1 workspace $ws11
bindsym $mod+Ctrl+2 workspace $ws12
bindsym $mod+Ctrl+3 workspace $ws13
bindsym $mod+Ctrl+4 workspace $ws14
bindsym $mod+Ctrl+5 workspace $ws15
bindsym $mod+Ctrl+6 workspace $ws16
bindsym $mod+Ctrl+7 workspace $ws17
bindsym $mod+Ctrl+8 workspace $ws18
bindsym $mod+Ctrl+9 workspace $ws19

# cycle across workspaces
#bindsym $mod+Tab workspace next
#bindsym $mod+Shift+Tab workspace prev

# Switch to the last workspace
bindsym $mod+Tab workspace back_and_forth
bindsym $alt+Tab workspace back_and_forth

# move focused container to workspace
bindsym $mod+Shift+1      move container to workspace $ws1
bindsym $mod+Shift+2      move container to workspace $ws2
bindsym $mod+Shift+3      move container to workspace $ws3
bindsym $mod+Shift+4      move container to workspace $ws4
bindsym $mod+Shift+5      move container to workspace $ws5
bindsym $mod+Shift+6      move container to workspace $ws6
bindsym $mod+Shift+7      move container to workspace $ws7
bindsym $mod+Shift+8      move container to workspace $ws8
bindsym $mod+Shift+9      move container to workspace $ws9
bindsym $mod+Shift+0      move container to workspace $ws10
bindsym $mod+Shift+Ctrl+1 move container to workspace $ws11
bindsym $mod+Shift+Ctrl+2 move container to workspace $ws12
bindsym $mod+Shift+Ctrl+3 move container to workspace $ws13
bindsym $mod+Shift+Ctrl+4 move container to workspace $ws14
bindsym $mod+Shift+Ctrl+5 move container to workspace $ws15
bindsym $mod+Shift+Ctrl+6 move container to workspace $ws16
bindsym $mod+Shift+Ctrl+7 move container to workspace $ws17
bindsym $mod+Shift+Ctrl+8 move container to workspace $ws18
bindsym $mod+Shift+Ctrl+9 move container to workspace $ws19

# move focused container to workspace and go there
bindsym $mod+$alt+1      move container to workspace $ws1;  workspace $ws1
bindsym $mod+$alt+2      move container to workspace $ws2;  workspace $ws2
bindsym $mod+$alt+3      move container to workspace $ws3;  workspace $ws3
bindsym $mod+$alt+4      move container to workspace $ws4;  workspace $ws4
bindsym $mod+$alt+5      move container to workspace $ws5;  workspace $ws5
bindsym $mod+$alt+6      move container to workspace $ws6;  workspace $ws6
bindsym $mod+$alt+7      move container to workspace $ws7;  workspace $ws7
bindsym $mod+$alt+8      move container to workspace $ws8;  workspace $ws8
bindsym $mod+$alt+9      move container to workspace $ws9;  workspace $ws9
bindsym $mod+$alt+0      move container to workspace $ws10; workspace $ws1
bindsym $mod+$alt+Ctrl+1 move container to workspace $ws11; workspace $ws11
bindsym $mod+$alt+Ctrl+2 move container to workspace $ws12; workspace $ws12
bindsym $mod+$alt+Ctrl+3 move container to workspace $ws13; workspace $ws13
bindsym $mod+$alt+Ctrl+4 move container to workspace $ws14; workspace $ws14
bindsym $mod+$alt+Ctrl+5 move container to workspace $ws15; workspace $ws15
bindsym $mod+$alt+Ctrl+6 move container to workspace $ws16; workspace $ws16
bindsym $mod+$alt+Ctrl+7 move container to workspace $ws17; workspace $ws17
bindsym $mod+$alt+Ctrl+8 move container to workspace $ws18; workspace $ws18
bindsym $mod+$alt+Ctrl+9 move container to workspace $ws19; workspace $ws19

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Ctrl+r restart

# exit the session
bindsym $mod+Shift+e exec /usr/bin/gnome-session-quit --power-off
#bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'Exit ?' -b 'Yes' '/usr/bin/gnome-session-quit --logout'"

# Reboot computer
#bindsym $mod+Shift+b exec /usr/bin/gnome-session-quit --reboot

# Power off computer
#bindsym $mod+Shift+p exec /usr/bin/gnome-session-quit --power-off

# put the system to sleep
#bindsym $mod+Shift+s exec systemctl suspend

# lock the screen
bindsym $mod+Escape exec gnome-screensaver-command --lock

# resize window (you can also use the mouse for that)
mode "Resize Mode" {
	# These bindings trigger as soon as you enter the resize mode

	set $size_change_small  2
	set $size_change_normal 5
	set $size_change_large 10

	# resize the focused window using the direction keys.
	# use shift for larger steps or alt for smaller
	bindsym $left        resize shrink width  $size_change_normal px or $size_change_normal ppt
	bindsym $down        resize grow   height $size_change_normal px or $size_change_normal ppt
	bindsym $up          resize shrink height $size_change_normal px or $size_change_normal ppt
	bindsym $right       resize grow   width  $size_change_normal px or $size_change_normal ppt

	bindsym Shift+$left  resize shrink width  $size_change_large  px or $size_change_large  ppt
	bindsym Shift+$down  resize grow   height $size_change_large  px or $size_change_large  ppt
	bindsym Shift+$up    resize shrink height $size_change_large  px or $size_change_large  ppt
	bindsym Shift+$right resize grow   width  $size_change_large  px or $size_change_large  ppt

	bindsym $alt+$left   resize shrink width  $size_change_small  px or $size_change_small  ppt
	bindsym $alt+$down   resize grow   height $size_change_small  px or $size_change_small  ppt
	bindsym $alt+$up     resize shrink height $size_change_small  px or $size_change_small  ppt
	bindsym $alt+$right  resize grow   width  $size_change_small  px or $size_change_small  ppt

	# same bindings, but for the arrow keys
	bindsym Left         resize shrink width  $size_change_normal px or $size_change_normal ppt
	bindsym Down         resize grow   height $size_change_normal px or $size_change_normal ppt
	bindsym Up           resize shrink height $size_change_normal px or $size_change_normal ppt
	bindsym Right        resize grow   width  $size_change_normal px or $size_change_normal ppt

	bindsym Shift+Left   resize shrink width  $size_change_large  px or $size_change_large  ppt
	bindsym Shift+Down   resize grow   height $size_change_large  px or $size_change_large  ppt
	bindsym Shift+Up     resize shrink height $size_change_large  px or $size_change_large  ppt
	bindsym Shift+Right  resize grow   width  $size_change_large  px or $size_change_large  ppt

	bindsym $alt+Left    resize shrink width  $size_change_small  px or $size_change_small  ppt
	bindsym $alt+Down    resize grow   height $size_change_small  px or $size_change_small  ppt
	bindsym $alt+Up      resize shrink height $size_change_small  px or $size_change_small  ppt
	bindsym $alt+Right   resize grow   width  $size_change_small  px or $size_change_small  ppt

	# back to normal
	bindsym Return mode "default"
	bindsym Escape mode "default"
	bindsym $mod+r mode "default"
}

bindsym $mod+r mode "Resize Mode"

# Disable titlebar
new_window pixel 1
new_float pixel 1

# Gaps (i3-gaps)
gaps inner 5
gaps outer 0

# Only enable gaps on a workspace when there is at least one container
smart_gaps on

set_from_resource $focused.color.border                i3-wm.client.focused.color.border                "#002b36"
set_from_resource $focused.color.background            i3-wm.client.focused.color.background            "#586e75"
set_from_resource $focused.color.text                  i3-wm.client.focused.color.text                  "#fdf6e3"
set_from_resource $focused.color.indicator             i3-wm.client.focused.color.indicator             "#268bd2"
set_from_resource $focused.color.child_border          i3-wm.client.focused.color.child_border

set_from_resource $focused_inactive.color.border       i3-wm.client.focused_inactive.color.border       "#002b36"
set_from_resource $focused_inactive.color.background   i3-wm.client.focused_inactive.color.background   "#073642"
set_from_resource $focused_inactive.color.text         i3-wm.client.focused_inactive.color.text         "#839496"
set_from_resource $focused_inactive.color.indicator    i3-wm.client.focused_inactive.color.indicator    "#073642"
set_from_resource $focused_inactive.color.child_border i3-wm.client.focused_inactive.color.child_border

set_from_resource $unfocused.color.border              i3-wm.client.unfocused.color.border              "#002b36"
set_from_resource $unfocused.color.background          i3-wm.client.unfocused.color.background          "#073642"
set_from_resource $unfocused.color.text                i3-wm.client.unfocused.color.text                "#839496"
set_from_resource $unfocused.color.indicator           i3-wm.client.unfocused.color.indicator           "#073642"
set_from_resource $unfocused.color.child_border        i3-wm.client.unfocused.color.child_border

set_from_resource $urgent.color.border                 i3-wm.client.urgent.color.border                 "#002b36"
set_from_resource $urgent.color.background             i3-wm.client.urgent.color.background             "#dc322f"
set_from_resource $urgent.color.text                   i3-wm.client.urgent.color.text                   "#fdf6e3"
set_from_resource $urgent.color.indicator              i3-wm.client.urgent.color.indicator              "#002b36"
set_from_resource $urgent.color.child_border           i3-wm.client.urgent.color.child_border

# Window Border color
# class                 border                         background                         text                         indicator                         child_border
client.focused          $focused.color.border          $focused.color.background          $focused.color.text          $focused.color.indicator          $focused.color.child_border
client.focused_inactive $focused_inactive.color.border $focused_inactive.color.background $focused_inactive.color.text $focused_inactive.color.indicator $focused_inactive.color.child_border
client.unfocused        $unfocused.color.border        $unfocused.color.background        $unfocused.color.text        $unfocused.color.indicator        $unfocused.color.child_border
client.urgent           $urgent.color.border           $urgent.color.background           $urgent.color.text           $urgent.color.indicator           $urgent.color.child_border

# Enable popup during fullscreen
popup_during_fullscreen smart

# window focus follows your mouse movements as the mouse crosses window borders
focus_follows_mouse yes

set_from_resource $i3-wm.bar.background.color                    i3-wm.bar.background.color                    "#002b36"
set_from_resource $i3-wm.bar.statusline.color                    i3-wm.bar.statusline.color                    "#93a1a1"
set_from_resource $i3-wm.bar.separator.color                     i3-wm.bar.separator.color                     "#268bd2"

set_from_resource $i3-wm.bar.workspace.focused.border.color      i3-wm.bar.workspace.focused.border.color      "#073642"
set_from_resource $i3-wm.bar.workspace.focused.background.color  i3-wm.bar.workspace.focused.background.color  "#073642"
set_from_resource $i3-wm.bar.workspace.focused.text.color        i3-wm.bar.workspace.focused.text.color        "#eee8d5"

set_from_resource $i3-wm.bar.workspace.active.border.color       i3-wm.bar.workspace.active.border.color       "#073642"
set_from_resource $i3-wm.bar.workspace.active.background.color   i3-wm.bar.workspace.active.background.color   "#073642"
set_from_resource $i3-wm.bar.workspace.active.text.color         i3-wm.bar.workspace.active.text.color         "#586e75"

set_from_resource $i3-wm.bar.workspace.inactive.border.color     i3-wm.bar.workspace.inactive.border.color     "#002b36"
set_from_resource $i3-wm.bar.workspace.inactive.background.color i3-wm.bar.workspace.inactive.background.color "#002b36"
set_from_resource $i3-wm.bar.workspace.inactive.text.color       i3-wm.bar.workspace.inactive.text.color       "#586e75"

set_from_resource $i3-wm.bar.workspace.urgent.border.color       i3-wm.bar.workspace.urgent.border.color       "#dc322f"
set_from_resource $i3-wm.bar.workspace.urgent.background.color   i3-wm.bar.workspace.urgent.background.color   "#dc322f"
set_from_resource $i3-wm.bar.workspace.urgent.text.color         i3-wm.bar.workspace.urgent.text.color         "#fdf6e3"

# Font for window titles
set_from_resource $i3-wm.bar.font i3-wm.bar.font "pango:Source Code Pro Medium 13, FontAwesome 13"

# Configure the bar
bar {
	font $i3-wm.bar.font
	separator_symbol " "
	status_command i3xrocks -c $i3xrocks_config
	position bottom
	tray_output none
	strip_workspace_numbers yes

	colors {
		background $i3-wm.bar.background.color
		statusline $i3-wm.bar.statusline.color
		separator  $i3-wm.bar.separator.color

		#                  border                                     background                                     text
		focused_workspace  $i3-wm.bar.workspace.focused.border.color  $i3-wm.bar.workspace.focused.background.color  $i3-wm.bar.workspace.focused.text.color
		active_workspace   $i3-wm.bar.workspace.active.border.color   $i3-wm.bar.workspace.active.background.color   $i3-wm.bar.workspace.active.text.color
		inactive_workspace $i3-wm.bar.workspace.inactive.border.color $i3-wm.bar.workspace.inactive.background.color $i3-wm.bar.workspace.inactive.text.color
		urgent_workspace   $i3-wm.bar.workspace.urgent.border.color   $i3-wm.bar.workspace.urgent.background.color   $i3-wm.bar.workspace.urgent.text.color
	}
}

# Run programs when i3 starts

# Monitor Setup
exec --no-startup-id xrandr --output DP-4 --primary --auto --right-of HDMI-0

# Initial Workspace Setup
workspace 1 output DP-1
workspace 3 output HDMI-1

# Set Sound to 100%
exec --no-startup-id amixer -D pulse sset Master 100%

# Set the Keyboard Input Module to IBus
exec --no-startup-id im-config -n ibus

# Start the composite manager
exec --no-startup-id compton -f --config $compton_config

# Hide the mouse pointer if unused for a duration
exec --no-startup-id /usr/bin/unclutter -b

# Refresh wallpaper on screen resolution change
exec --no-startup-id /usr/bin/xeventbind resolution /usr/bin/wallpaper-refresh

# Start network-manager applet
exec --no-startup-id /usr/bin/nm-applet --sm-disable

